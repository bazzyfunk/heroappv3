package com.crea.heroappv3.utils;

import android.util.Log;

public class Util {

    public static void logTag(String tag, String message){
        Log.d(tag, message);
    }
}
