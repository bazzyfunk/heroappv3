package com.crea.heroappv3.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.crea.heroappv3.R;
import com.crea.heroappv3.model.Hero;
import com.squareup.picasso.Picasso;


public class HeroProfilFragment extends Fragment {
    private Hero hero;

    public  HeroProfilFragment(Hero hero){
        this.hero = hero;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personal_id, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView name = view.findViewById(R.id.HeroNameProfil);
        TextView realname = view.findViewById(R.id.heroRealName);
        TextView power = view.findViewById(R.id.heroPower);
        ImageView image = view.findViewById(R.id.HeroImageProfile);

        Picasso.get().load(hero.getImage()).into(image);

        name.setText(hero.getName());
        realname.setText(hero.getFullname());
        power.setText(hero.getPower());




    }
}
