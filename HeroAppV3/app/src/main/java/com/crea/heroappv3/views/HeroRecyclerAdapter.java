package com.crea.heroappv3.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.crea.heroappv3.activities.MainActivity;
import com.crea.heroappv3.fragments.HeroProfilFragment;
import com.crea.heroappv3.fragments.UtilNavigationFragment;
import com.crea.heroappv3.model.Hero;
import com.crea.heroappv3.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HeroRecyclerAdapter extends RecyclerView.Adapter<HeroRecyclerAdapter.CellViewHolder> {


    private ArrayList<Hero> heros;

    private FragmentActivity context;

    public HeroRecyclerAdapter(ArrayList<Hero> heros, FragmentActivity context) {
        this.heros = heros;
        this.context = context;
    }

    public static class CellViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;
        LinearLayout linearLayout;
        public CellViewHolder(View v) {
            super(v);

            imageView = v.findViewById(R.id.cellViewImageView);


            textView = v.findViewById(R.id.cellViewHeroName);

            linearLayout = v.findViewById(R.id.CellLayout);


        }

    }
    @NonNull
    @Override
    public CellViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(MainActivity.MAIN_CONTEXT).inflate(R.layout.cell_view, parent, false);

        CellViewHolder viewHolder = new CellViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CellViewHolder holder, int position) {
        final Hero hero = this.heros.get(position);

        holder.textView.setText(hero.getName());

        Picasso.get().load(hero.getImage()).into(holder.imageView);

        // Go to the profil page of this specific hero
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goToUniqueHeroPage(hero);
            }
        });



    }

    private void goToUniqueHeroPage(Hero hero) {
        HeroProfilFragment heroProfilFragment = new HeroProfilFragment(hero);

        UtilNavigationFragment.nextFragment(heroProfilFragment, this.context );
    }

    @Override
    public int getItemCount() {
        return this.heros.size();
    }


}
