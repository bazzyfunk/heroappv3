package com.crea.heroappv3.interfaces;

import com.crea.heroappv3.model.Hero;

import java.util.ArrayList;

public interface HeroServiceListener {

    public void responseWithSuccess(ArrayList<Hero> heros);
    public void responseWithError(String errorMesssage);
}
